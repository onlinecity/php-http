<?php namespace Onlinecity\Http\Tests;

use Onlinecity\Http\Request;

class RequestTest extends \PHPUnit_Framework_TestCase
{
  const REQUEST_TEST_URL = 'http://localhost/index.php/namespace/controller/action/?parameter1=value1';

  public function testSetFromRequest()
  {
    $_SERVER['REQUEST_URI'] = str_replace('http://localhost', '//', self::REQUEST_TEST_URL);
    $this->testSetFromUrl(null);
  }

  public function testSetFromUrl($url = self::REQUEST_TEST_URL)
  {
    $request = new Request($url);
    $this->assertEquals('/index.php/namespace/controller/action', $request->getPath());
    $this->assertEquals('namespace/controller/action', $request->getPathInfo());
    $this->assertEquals(array('namespace', 'controller', 'action'), $request->getPathArray());
    $this->assertEquals('parameter1=value1', $request->getQueryString());
    $this->assertEquals('value1', $request->getQuery('parameter1'));
    $this->assertEquals(1, count($request->getQueryArray()));
  }
}
