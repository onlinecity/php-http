<?php namespace Onlinecity\Http;

/**
 * Request
 *
 * Elegant and unobtrusive http request handler with middleware support.
 *
 * - Use your own custom url, handy when doing tests
 * - RESTful data support, see getData methods.
 *
 * @package Onlinecity\Http
 * @author Jari Berg <jb@onlinecity.dk>
 */
class Request
{
  /**
   * Scheme
   * @var string
   */
  protected $scheme;

  /**
   * Host
   * @var string
   */
  protected $host;

  /**
   * Path
   * @var string
   */
  protected $path;

  /**
   * Paths values from url
   * /user/list-users => ['user', 'list-users']
   * @var array
   */
  protected $pathValues;

  /**
   * Pathinfo
   * @var string
   */
  protected $pathInfo;

  /**
   * Query string
   * @var string
   */
  protected $query;

  /**
   * Query as key => value pairs
   * @var array
   */
  protected $queryValues;

  /**
   * Data as key => value pairs
   * @var array
   */
  protected $dataValues;

  /**
   * Http method
   * @var string
   */
  protected $httpMethod;

  /**
   * Constructor
   *
   * @param string $url [optional] Optional url, leave empty to autodetect
   * @param string $httpMethod [optional] Option to override http method (GET, PUT, UDPATE, DELETE), leave empty to autodetect
   */
  public function __construct($url = null, $httpMethod = null)
  {
    if ($url) {
      $this->setFromUrl($url);
    } else {
      $this->setFromRequest();
    }
    if ($httpMethod) {
      $this->setHttpMethod($httpMethod);
    }
  }

  /**
   * Whether request accept json
   *
   * @return bool
   */
  public function acceptJson()
  {
    return stripos($this->getServer('HTTP_ACCEPT'), 'application/json') !== false;
  }

  /**
   * Build key => value pairs from request data (POST, PUT, DELETE...)
   *
   * @return Request
   */
  protected function buildData()
  {
    $this->dataValues = isset($_POST) ? $_POST : array();

    // try to build RESTful data if no _POST data was found
    if (empty($this->dataValues) && $this->acceptJson()) {
      $content = file_get_contents('php://input');
      if ($this->isJson() && $content && ($values = @json_decode($content, true))) {
        // success, got decoded json data
      } elseif ($content) {
        $values = array();
        @parse_str($content, $values);
      }
      if (isset($values) && is_array($values)) {
        $this->dataValues = $values;
      }
    }

    return $this;
  }

  /**
   * Get data value if exists, null otherwise
   *
   * Data values are build from _POST and RESTful content if available
   *
   * @param string $name
   *
   * @return string|null
   */
  public function getData($name)
  {
    if (array_key_exists($name, $this->dataValues)) {
      return $this->dataValues[$name];
    } else {
      return null;
    }
  }

  /**
   * Get array of data values
   *
   * Data values are build from _POST and RESTful content if available
   *
   * @return array
   */
  public function getDataArray()
  {
    return $this->dataValues;
  }

  /**
   * Get host
   * @return string
   */
  public function getHost()
  {
    return $this->host;
  }

  /**
   * Get HTTP Request Method supporting X-HTTP-Method-Override
   * @return string
   */
  public function getMethod()
  {
    if ($this->httpMethod) {
      return $this->httpMethod;
    } else {
      return $this->getServer('HTTP_X_HTTP_METHOD_OVERRIDE') ? : $this->getServer('REQUEST_METHOD');
    }
  }

  /**
   * Get path
   * @return string
   */
  public function getPath()
  {
    return $this->path;
  }

  /**
   * Get array of pathinfo values
   * @return array
   */
  public function getPathArray()
  {
    return $this->pathValues;
  }

  /**
   * Get pathinfo
   * @return string
   */
  public function getPathInfo()
  {
    return $this->pathInfo;
  }

  /**
   * Get query value if exists, null otherwise
   *
   * @param string $name
   *
   * @return string|null
   */
  public function getQuery($name)
  {
    if (array_key_exists($name, $this->queryValues)) {
      return $this->queryValues[$name];
    } else {
      return null;
    }
  }

  /**
   * Get query string
   */
  public function getQueryString()
  {
    return $this->query;
  }

  /**
   * Get array of query values
   * @return array
   */
  public function getQueryArray()
  {
    return $this->queryValues;
  }

  /**
   * Get scheme
   *
   * @return string
   */
  public function getScheme()
  {
    return $this->scheme;
  }

  /**
   * Get server value if exists, null otherwise
   *
   * @param string $name
   *
   * @return string|null
   */
  public function getServer($name)
  {
    if (array_key_exists($name, $_SERVER)) {
      return $_SERVER[$name];
    } else {
      return null;
    }
  }

  /**
   * Get url
   * @return string
   */
  public function getUrl()
  {
    return $this->getScheme() . '://' . $this->getHost() . $this->getPath() . '/' . $this->getQueryString();
  }

  /**
   * Whether request is json
   *
   * @return bool
   */
  public function isJson()
  {
    return stripos($this->getServer('CONTENT_TYPE'), 'application/json') !== false;
  }

  /**
   * Whether request is xhr (ajax)
   * @return bool
   */
  public function isAjax()
  {
    return $this->getServer('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest';
  }

  /**
   * Whether request is secure
   * @return bool
   */
  public function isSecure()
  {
    return mb_substr($this->getUrl(), 0, 8) == 'https://';
  }

  /**
   * Set path, query and data from request
   */
  public function setFromRequest()
  {
    $this->setHost(false); // false to autodetect
    $this->setScheme(false); // false to autodetect
    $this->setQuery(false); // false to autodetect
    $this->setPath(false); // false to autodetect

    $this->buildData();

    return $this;
  }

  /**
   * Set scheme, host, path and query from url string
   *
   * @param string $url
   * @param string $httpMethod [optional] The http method (GET, PUT, UDPATE, DELETE), leave empty to autodetect
   *
   * @return Request
   */
  public function setFromUrl($url, $httpMethod = null)
  {
    if ($httpMethod) {
      $this->setHttpMethod($httpMethod);
    }

    $parsed = parse_url(ltrim($url, '/'));

    $parsed = array(
      'host' => isset($parsed['host']) ? $parsed['host'] : null,
      'scheme' => isset($parsed['scheme']) ? $parsed['scheme'] : null,
      'query' => isset($parsed['query']) ? $parsed['query'] : null,
      'path' => isset($parsed['path']) ? $parsed['path'] : null
    );

    $this->setHost($parsed['host']);
    $this->setScheme($parsed['scheme']);
    $this->setQuery($parsed['query']);
    $this->setPath($parsed['path']);

    $this->buildData();

    return $this;
  }

  /**
   * Set http method
   *
   * @param string $httpMethod
   *
   * @return Request
   */
  protected function setHttpMethod($httpMethod)
  {
    if ($httpMethod && in_array(
        $httpMethod = strtoupper($httpMethod),
        array(
          'GET',
          'PUT',
          'POST',
          'UPDATE',
          'DELETE',
          'PATCH',
          'OPTIONS',
          'HEAD',
          'TRACE',
          'CONNECT'
        )
      )
    ) {
      $this->httpMethod = $httpMethod;
    }
    return $this;
  }

  /**
   * Set host if non empty otherwise try to autodetect
   *
   * @param string $host
   *
   * @return Request
   */
  protected function setHost($host)
  {
    // if false, try to autodetect from request
    if ($host === false && $host = $this->getServer('HTTP_HOST')) {
      // success, got host from HTTP_HOST
    }

    $this->host = $host ? : 'localhost';

    return $this;
  }

  /**
   * Set path if non empty otherwise try to autodetect
   *
   * @param string $path
   *
   * @return Request
   */
  protected function setPath($path)
  {
    // if false, try to autodetect from request
    if ($path === false) {
      $path = parse_url(ltrim($this->getServer('REQUEST_URI'), '/'), PHP_URL_PATH);
    }

    $this->path = '/' . trim($path, '/ ');

    /**
     * Build paths array and pathinfo from path.
     * Pathinfo is cleaning from empty path values.
     * Path values before script-based rewrite's (app.php/demo/demo)
     * is also removed to support hand crafted urls
     *
     * http://demo.com/users/listusers?page=1 => users/listusers
     * http://localhost/demo/index.php/users/listusers?page=1 => users/listusers
     */

    $pathinfo = explode('.php/', $this->path);

    // if "index.php" was found we have pathinfo at index 1 in the pathinfo array
    if (isset($pathinfo[1])) {
      $pathinfo = $pathinfo[1];
    } else {
      $pathinfo = $pathinfo[0];
    }

    // we now have pretty clean paths
    $this->pathValues = explode('/', trim($pathinfo, '/'));

    // build pathinfo from clean paths
    $this->pathInfo = implode('/', $this->pathValues);

    return $this;
  }

  /**
   * Set scheme
   *
   * @param string $scheme
   *
   * @return Request
   */
  protected function setScheme($scheme)
  {
    // if false, try to autodetect from request
    if ($scheme === false) {
      if ($this->getServer('HTTPS') && $this->getServer('HTTPS') !== 'off') {
        // success, got scheme from HTTPS
        $scheme = 'https';
      } elseif ($scheme = strtolower($this->getServer('REQUEST_SCHEME'))) {
        // success, got scheme from REQUEST_SCHEME
      } else {
        $scheme = 'http';
      }
    } else {
      $scheme = strtolower($scheme);
    }

    $this->scheme = $scheme ? : 'http';

    return $this;
  }

  /**
   * Set query string and build query values
   *
   * @param string $query
   *
   * @return Request
   */
  protected function setQuery($query)
  {
    // if false, try to autodetect from request
    if ($query === false) {
      if ($query = $this->getServer('REDIRECT_QUERY_STRING')) {
        // success, got query from REDIRECT_QUERY_STRING
      } elseif ($query = $this->getServer('QUERY_STRING')) {
        // success, got query from QUERY_STRING
      } elseif ($query = parse_url(ltrim($this->getServer('REQUEST_URI'),'/'), PHP_URL_QUERY)) {
        // success, got query from REQUEST_URI
      }
    }

    if ($query) {
      $this->query = $query;
    }

    // build key => value pairs from query string
    $this->queryValues = array();
    parse_str($this->query, $this->queryValues);

    return $this;
  }

  /**
   * Move uploaded files to directory
   *
   * @param string $dir
   *
   * @throws \RuntimeException
   */
  public function moveUploadedFiles($dir)
  {
    foreach ((array)$_FILES as $file) {
      if (false === isset($file['tmp_name']) || false === is_uploaded_file($file['tmp_name'])) {
        continue;
      }
      if (false === is_writable($dir)) {
        throw new \RuntimeException(sprintf('Failed to move uploaded files. Target directory "%s" is not writable', $dir));
      }
      if (false === isset($file['error']) || $file['error'] != UPLOAD_ERR_OK) {
        $errorname = array_search($file['error'], get_defined_constants(), true);
        throw new \RuntimeException(sprintf('Failed to upload file "%s" with error: "%s"', $file['name'], $errorname));
      }
      move_uploaded_file($file['tmp_name'], $dir . DIRECTORY_SEPARATOR . $file['name']);
    }
  }
}